# gnome-shell-extension-custom-accent-colors

Set a custom accent color for GTK4/GTK3 apps and GNOME Shell

https://github.com/demiskp/custom-accent-colors

<br><br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/gnome/extensions/gnome-shell-extension-custom-accent-colors.git
```

